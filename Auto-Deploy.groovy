def getGitBranchName() {
    println("On branch " + scm.branches[0].name)
    return scm.branches[0].name
}


def GIT_CRED = '95a9e1de-023e-4078-8018-2d04d77ad7e8'
def LIB_ID = 'autoDeploy@' + getGitBranchName()
def deployNode

library identifier: LIB_ID, retriever: modernSCM([
	$class: 'GitSCMSource',
	remote: 'git@gitlab:compis/compis-iac.git',
	credentialsId: GIT_CRED
    ])

if (params.DEPLOYNODE != null &&
    params.DEPLOYNODE.length()>0 ) {
    deployNode = unstashParam("DEPLOYNODE")
} else {
    deployNode = 'machines_deployment_slaves'
}

node (deployNode) {
    stage ('initialize autoDeploy API') {
	autoDeploy()
	autoDeploy.setApiBranch(getGitBranchName())
	autoDeploy.hello()
    }
    stage ('general preparation') {
	autoDeploy.setVcenterUser(params.USERNAME)
	autoDeploy.setVcenterPassword(params.VCENTER_PASSWORD.toString())
	autoDeploy.setApiBranch(getGitBranchName())
    }
    stage('unified auto deploy wrapper') {
	autoDeploy.unifiedAutoDeploy()
    }
}
;
