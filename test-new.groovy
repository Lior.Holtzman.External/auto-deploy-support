def getGitBranchName() {
    println("On branch " + scm.branches[0].name)
    return scm.branches[0].name
}


def GIT_CRED = '95a9e1de-023e-4078-8018-2d04d77ad7e8'
def LIB_ID = 'autoDeploy@' + getGitBranchName()
def deployNode

library identifier: LIB_ID, retriever: modernSCM([
	$class: 'GitSCMSource',
	remote: 'git@gitlab:compis/compis-iac.git',
	credentialsId: GIT_CRED
    ])

if (params.DEPLOYNODE != null &&
    params.DEPLOYNODE.length()>0 ) {
    deployNode = unstashParam("DEPLOYNODE")
} else {
    deployNode = 'machines_deployment_slaves'
}


IPSLIST =  env.IPSLIST.tokenize('\n')
SSBUILD =  env.SSBUILD.tokenize('.')
PATCH = env.PATCH.tokenize('.')
GENERICPASSWORD = env.GENERICPASSWORD
switch (env.PATCH) {
  case ~/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/:
  VERSION = "${SSBUILD[0]}.${PATCH[0]}"
  ARG = "${VERSION} ${PATCH[1]}.${PATCH[2]}";break;
  default:
  VERSION = "${SSBUILD[0]}.${SSBUILD[1]}"
  ARG = "${VERSION} ${env.PATCH}";break;
}

switch (env.PATCH) {
  case ~/([0-9]{1,3}\.[0-9]{1,3}.*)/:
  println "Installting Patch"
  SCRIPT='''
passwd
root123
root123
if [[ $(cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="server"') ]] ;then impctl db stop;impctl server stop;impctl db config --password=system12;impctl server config --password=secure12;impctl db start;impctl server start;fi
if [[ $( cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="gw"') ]] ;then impctl gateway stop;impctl server config --password=secure12 ;sed -i 's/valid_pam_password/true;#valid_pam_password/g' /opt/SecureSphere/etc/impctl/lib/gateway.sh;impctl gateway password config --password=secure12
fi
if [[ $( cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="gw-and-server"') ]] ;then impctl gateway stop;impctl server config --password=secure12 ;impctl db stop;impctl server stop;impctl db config --password=system12;sed -i 's/valid_pam_password/true;#valid_pam_password/g' /opt/SecureSphere/etc/impctl/lib/gateway.sh;impctl gateway password config --password=secure12;impctl db start;impctl server start
fi
if [[ $( cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="gw"') ]] || [[ $(cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="gw-and-server"') ]]
then
impctl service stop  --teardown --transient gateway
impctl gateway unregister
impctl gateway config --mode=bridge-impvha
impctl gateway unregister
impctl gateway bridge-impvha delete --all
impctl gateway bridge-impvha config --name=br0 --device=eth1 --device=eth2 --high-availability=false
MXIP="$(impctl gateway show | grep server-address | awk -F "                        " '{printf $2}')"
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/auth/session  --header "Content-Type: application/json" --header "Authorization: Basic YWRtaW46YWRtaW4xMg==" > /var/tmp/APIsession_id.txt
SESSIONID="$(cat /var/tmp/APIsession_id.txt | awk -F '"' '{printf$4}')"
SITE=Auto_Site;SG=Auto_SeverGruop;SV=Auto_HTTP;GP=`hostname`
PIPHTTP1="$(ifconfig | sed -n 2p | awk -F " " '{printf$2}' | awk -F ":" '{printf$2}' | gawk -F "." '{printf$1"."$2"."$4-1"."101}')"
PIPHTTP2="$(ifconfig | sed -n 2p | awk -F " " '{printf$2}' | awk -F ":" '{printf$2}' | gawk -F "." '{printf$1"."$2"."$4-1"."102}')"
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/sites/$SITE --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"comment" : "Some Comment"}'
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/serverGroups/$SITE/$SG --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"comment" : "Some Comment"}'
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/webServices/$SITE/$SG/$SV --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"ports" : [80]}'
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/serverGroups/$SITE/$SG/protectedIPs/$PIPHTTP1?gatewayGroup=$GP --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"comment" : "This IP was added by Auto Deploy"}'
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/serverGroups/$SITE/$SG/protectedIPs/$PIPHTTP2?gatewayGroup=$GP --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"comment" : "This IP was added by Auto Deploy"}'
fi
wget http://10.100.10.100/esc.x; bash esc.x
VERSION=$1
PATCH=$2
bash -x ss_install_patch.sh -v $1 -p $2 2>/root/PatchInstalltion.txt
y
y
reboot
''';break;
  default:
  println "Patch installtion is not reqiuerd"
  SCRIPT='''
passwd
root123
root123
if [[ $(cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="server"') ]] ;then impctl db stop;impctl server stop;impctl db config --password=system12 ;impctl server config --password=secure12;impctl db start;impctl server start;fi
if [[ $( cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="gw"') ]] ;then impctl gateway stop;impctl server config --password=secure12 ;sed -i 's/valid_pam_password/true;#valid_pam_password/g' /opt/SecureSphere/etc/impctl/lib/gateway.sh;impctl gateway password config --password=secure12
fi
if [[ $( cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="gw-and-server"') ]] ;then impctl gateway stop;impctl server config --password=secure12 ;impctl db stop;impctl server stop;impctl db config --password=system12;sed -i 's/valid_pam_password/true;#valid_pam_password/g' /opt/SecureSphere/etc/impctl/lib/gateway.sh;impctl gateway password config --password=secure12;impctl db start;impctl server start
fi
if [[ $( cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="gw"') ]] || [[ $(cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="gw-and-server"') ]]
then
impctl service stop  --teardown --transient gateway
impctl gateway unregister
impctl gateway config --mode=bridge-impvha
impctl gateway unregister
impctl gateway bridge-impvha delete --all
impctl gateway bridge-impvha config --name=br0 --device=eth1 --device=eth2 --high-availability=false
impctl service start --prepare  --transient gateway
MXIP="$(impctl gateway show | grep server-address | awk -F "                        " '{printf $2}')"
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/auth/session  --header "Content-Type: application/json" --header "Authorization: Basic YWRtaW46YWRtaW4xMg==" > /var/tmp/APIsession_id.txt
SESSIONID="$(cat /var/tmp/APIsession_id.txt | awk -F '"' '{printf$4}')"
SITE=Auto_Site;SG=Auto_SeverGruop;SV=Auto_HTTP;GP=`hostanme`
PIPHTTP1="$(ifconfig | sed -n 2p | awk -F " " '{printf$2}' | awk -F ":" '{printf$2}' | gawk -F "." '{printf$1"."$2"."$4-1"."101}')"
PIPHTTP2="$(ifconfig | sed -n 2p | awk -F " " '{printf$2}' | awk -F ":" '{printf$2}' | gawk -F "." '{printf$1"."$2"."$4-1"."102}')"
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/sites/$SITE --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"comment" : "Some Comment"}'
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/serverGroups/$SITE/$SG --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"comment" : "Some Comment"}'
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/webServices/$SITE/$SG/$SV --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"ports" : [80]}'
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/serverGroups/$SITE/$SG/protectedIPs/$PIPHTTP1?gatewayGroup=$GP --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"comment" : "This IP was added by Auto Deploy"}'
curl -k -X POST https://$MXIP:8083/SecureSphere/api/v1/conf/serverGroups/$SITE/$SG/protectedIPs/$PIPHTTP2?gatewayGroup=$GP --header "Content-Type: application/json" --header "Cookie: $SESSIONID"  --data '{"comment" : "This IP was added by Auto Deploy"}'
fi
wget http://10.100.10.100/esc.x; bash esc.x
''';
}

LICENSE='''
if [[ $(cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="server"') ]] || [[ $( cat /opt/SecureSphere/etc/bootstrap.xml | grep 'active-components="gw-and-server"') ]]
then
echo client.include.test.cpt = false >> ~mxserver/SecureSphere/jakarta-tomcat-secsph/webapps/SecureSphere/WEB-INF/bootstrap.properties
sed -i "s/<value>60000<\\/value>/<value>0<\\/value>/g" /opt/SecureSphere/server/SecureSphere/jakarta-tomcat-secsph/webapps/SecureSphere/WEB-INF/conf/appcontext/web-context.xml
impctl platform config --asset-tag=VM150
else
echo -e "Not MX"
exit
fi
'''

def runBashcommands(commands) {
  for (IP in IPSLIST) {
    sh '''sshpass -p '''+GENERICPASSWORD+''' ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@'''+IP+''' "bash -s '''+ARG+'''"<<'EOF'
    '''+commands+'''EOF'''
  }
}

node (deployNode) {
    stage ('initialize autoDeploy API') {
	autoDeploy()
	autoDeploy.setApiBranch(getGitBranchName())
	autoDeploy.hello()
    }
    stage ('general preparation') {
	autoDeploy.setVcenterUser(params.USERNAME)
	autoDeploy.setVcenterPassword(params.VCENTER_PASSWORD.toString())
	autoDeploy.setApiBranch(getGitBranchName())
    }
    stage('unified auto deploy wrapper') {
	autoDeploy.unifiedAutoDeploy()
    }
    stage('Uploading License') {
    runBashcommands(LICENSE)
	  env.LOAD_MAGIC_LICENSE == "true"
    }
    stage('Support Script') {
    runBashcommands(SCRIPT)
    println "Done :)"
    }
}
;
